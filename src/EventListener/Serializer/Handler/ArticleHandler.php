<?php


namespace App\EventListener\Serializer\Handler;


use App\Entity\Article;
use JMS\Serializer\Context;
use JMS\Serializer\GraphNavigator;
use JMS\Serializer\GraphNavigatorInterface;
use JMS\Serializer\Handler\SubscribingHandlerInterface;
use JMS\Serializer\JsonDeserializationVisitor;
use JMS\Serializer\JsonSerializationVisitor;

class ArticleHandler implements SubscribingHandlerInterface
{


    /**
     * Return format:
     *
     *      array(
     *          array(
     *              'direction' => GraphNavigatorInterface::DIRECTION_SERIALIZATION,
     *              'format' => 'json',
     *              'type' => 'DateTime',
     *              'method' => 'serializeDateTimeToJson',
     *          ),
     *      )
     *
     * The direction and method keys can be omitted.
     *
     * @return array
     *
     * @phpcsSuppress SlevomatCodingStandard.TypeHints.TypeHintDeclaration.MissingReturnTypeHint
     */
    public static function getSubscribingMethods()
    {
        return [
            [
                'direction' => GraphNavigatorInterface::DIRECTION_SERIALIZATION,
                'format' => 'json',
                'type' => Article::class,
                'method' => 'serialize'
            ],

            [
                'direction' => GraphNavigator::DIRECTION_DESERIALIZATION,
                'format' => 'json',
                'type' => 'AppBundle\Entity\Article',
                'method' => 'deserialize',
            ]

        ];
    }


    public static function serialize(JsonSerializationVisitor $visitor, Article $article, array $type, Context $context)
    {

        $date = new \DateTime();

        $data = [
            'title' => $article->getTitle(),
            'content' => $article->getContent(),
            'seralized_atTest3' => $date->format('l jS \of F Y h:i:s A')

        ];

        return $visitor->visitArray($data, $type, $context); //accessible depuis l'objet JsonSerializerVisitor ( $visitor) permettant de parcourir ce tableau et s'assurer qu'il est correct.

    }


    public function deserialize(JsonDeserializationVisitor $visitor, $data)
    {
        return new Article($data);
    }
}