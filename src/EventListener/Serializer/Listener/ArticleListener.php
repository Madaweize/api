<?php


namespace App\EventListener\Serializer\Listener;


use App\Entity\Article;
use JMS\Serializer\EventDispatcher\Events;
use JMS\Serializer\EventDispatcher\EventSubscriberInterface;
use JMS\Serializer\EventDispatcher\ObjectEvent;
use JMS\Serializer\Metadata\StaticPropertyMetadata;

class ArticleListener implements EventSubscriberInterface
{

    /**
     * Returns the events to which this class has subscribed.
     *
     * Return format:
     *     array(
     *         array('event' => 'the-event-name', 'method' => 'onEventName', 'class' => 'some-class', 'format' => 'json'),
     *         array(...),
     *     )
     *
     * The class may be omitted if the class wants to subscribe to events of all classes.
     * Same goes for the format key.
     *
     * @return array
     *
     * @phpcsSuppress SlevomatCodingStandard.TypeHints.TypeHintDeclaration.MissingReturnTypeHint
     */
    public static function getSubscribedEvents()
    {
        return [
           [
               'event' => Events::POST_SERIALIZE,
               'format' => 'json',
               'class' => Article::class,
               'method' => 'onPostSerialize'
           ]

        ];
    }



    public static function onPostSerialize(ObjectEvent $event){

        $date = new \DateTime();

        $visitor = $event->getVisitor();
        $visitor->visitProperty(new StaticPropertyMetadata('', 'serialized_atTest1', null),$date->format('l jS \of F Y h:i:s A'));

    }
}