<?php

namespace App\Controller;

use App\Entity\Livre;
use App\Exception\ResourceNotFoundException;
use App\Exception\ResourceValidationException;
use App\Form\LivreType;
use App\Service\WeatherService;
use FOS\RestBundle\Controller\AbstractFOSRestController;
use FOS\RestBundle\Request\ParamFetcherInterface;
use JMS\Serializer\Serializer;
use JMS\Serializer\SerializerInterface;
use Knp\Component\Pager\Pagination\PaginationInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\Config\Definition\Exception\Exception;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;



use FOS\RestBundle\Controller\Annotations\Get;
use FOS\RestBundle\Controller\Annotations\View;
use FOS\RestBundle\Controller\Annotations\Post;
USE FOS\RestBundle\Controller\Annotations\Delete;
use FOS\RestBundle\Controller\Annotations\Put;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\ParamConverter;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use FOS\RestBundle\Controller\Annotations\QueryParam;
use FOS\RestBundle\Controller\Annotations\RequestParam;

use Knp\Component\Pager\PaginatorInterface;
use Symfony\Component\Translation\Exception\NotFoundResourceException;
use Symfony\Component\Validator\ConstraintViolationList;
use Symfony\Component\Validator\Validator\ValidatorInterface;
use Symfony\Contracts\HttpClient\HttpClientInterface;

// @View(serializerGroups={"list"})

/**
 * Class LivreController
 * @package App\Controller
 * @Route("/api")
 */
class LivreController extends AbstractFOSRestController
{

    /**
     * @Get(
     *     path = "/livres/{id}",
     *     name = "livre_show",
     *     requirements = {"id"="\d+"}
     * )
     * @View
     * @param Request $request
     * @return object|null
     */
    public function show(Request $request)
    {

        $id = $request->get('id');
        $livre = $this->getDoctrine()->getRepository(Livre::class)->find($id);


        if(!empty($livre)){
            return $livre;
        }else{
            return $this->view("Cette ressource n'existe pas",Response::HTTP_NOT_FOUND);
        }

    }


    /**
     * @PUT(
     *     path = "/livres",
     *     name = "livre_update"
     * )
     * @ParamConverter(
     *     "livre",
     *      converter="fos_rest.request_body"
     *     )
     * @param Livre $livre
     * @return \FOS\RestBundle\View\View
     */
    public function update(Livre $livre){


        $em = $this->getDoctrine()->getManager();
        $oldLivre = $this->getDoctrine()->getRepository(Livre::class)->find($livre->getId());

        $oldLivre->setTitle($livre->getTitle());
        $oldLivre->setContent($livre->getContent());

        $em->flush();

        return $this->view(
            $oldLivre,
            Response::HTTP_OK,
            [
                'Location' => $this->generateUrl('livre_show',['id' =>$livre->getId()],UrlGeneratorInterface::ABSOLUTE_URL)
            ]
        );

    }

    //Control du parametre envoyé en GET
    /**
     * @DELETE(
     *     path = "/livres/{id}",
     *     name = "livre_delete",
     *     requirements = {"id"="\d+"}
     * )
     * @View
     * @param Request $request
     * @return object|null
     */
    public function delete(Request $request)
    {

        $id = $request->get('id');
        $livre = $this->getDoctrine()->getRepository(Livre::class)->find($id);

        $em = $this->getDoctrine()->getManager();

        if(!empty($livre)){
            $em->remove($livre);
            $em->flush();
        }else{
            return $this->view("Cette ressource n'existe pas",Response::HTTP_NOT_FOUND);
        }

        return $this->view($livre,Response::HTTP_OK);

    }

    /**
     * @Post(
     *     path = "/livres",
     *     name = "livre_create"
     * )
     * @ParamConverter(
     *     "livre",
     *      converter="fos_rest.request_body",
     *     options = {
     *                 "validator" = {"groups" = "Create"}
     *                    }
     *     )
     * @param Livre $livre
     * @param ValidatorInterface $validator
     * @param ConstraintViolationList $violations
     * @return \FOS\RestBundle\View\View
     */
    public function create(Livre $livre,ValidatorInterface $validator,ConstraintViolationList $violations)
    {

     //Mage sans la configuration fost_rest :validate
      /*  $error = $validator->validate($livre);

        if (count($error)){
            return $this->view($error,Response::HTTP_BAD_REQUEST);
        }*/
        if (count($violations)){
            return $this->view($violations,Response::HTTP_BAD_REQUEST);
        }

        $em = $this->getDoctrine()->getManager();
        $em->persist($livre);
        $em->flush();

        return $this->view(
            $livre,
            Response::HTTP_CREATED,
            [
                'Location' => $this->generateUrl('livre_show',['id' =>$livre->getId()],UrlGeneratorInterface::ABSOLUTE_URL)
            ]
        );

    }


    /**
     * @Post(
     *     path = "/livresseconde",
     *     name = "livre_createsecond"
     * )
     * @View(StatusCode = 201)
     *
     * @param Request $request
     * @param SerializerInterface $serializer
     * @return \FOS\RestBundle\View\View
     */
    public function createSecond(Request $request,SerializerInterface $serializer)
    {

        $data = $serializer->deserialize($request->getContent(), 'array', 'json');
        $livre = new Livre();
        $form = $this->get('form.factory')->create(LivreType::class, $livre);
        $form->submit($data);

        $em = $this->getDoctrine()->getManager();

        $em->persist($livre);
        $em->flush();


        return $this->view(
            $livre,
            Response::HTTP_CREATED,
            [
                'Location' => $this->generateUrl('livre_show',['id' =>$livre->getId()],UrlGeneratorInterface::ABSOLUTE_URL)
            ]
        );

    }


    /**
     * @Post(
     *     path = "/livresTree",
     *     name = "livre_createTree"
     * )
     * @View(StatusCode = 201)
     *
     * @param Request $request
     * @param SerializerInterface $serializer
     * @return \FOS\RestBundle\View\View
     */
    public function createTree(Request $request,SerializerInterface $serializer)
    {

        $livre= $serializer->deserialize($request->getContent(), Livre::class, 'json');

        $em = $this->getDoctrine()->getManager();
        $em->persist($livre);
        $em->flush();


        return $this->view(
            $livre,
            Response::HTTP_CREATED,
            [
                'Location' => $this->generateUrl('livre_show',['id' =>$livre->getId()],UrlGeneratorInterface::ABSOLUTE_URL)
            ]
        );

    }



    /**
     * @Post(
     *     path = "/livres_gestion_erreur",
     *     name = "livre_create_gestion_erreur"
     * )
     * @ParamConverter(
     *     "livre",
     *      converter="fos_rest.request_body",
     *     options = {
     *                 "validator" = {"groups" = "Create"}
     *                    }
     *     )
     * @param Livre $livre
     * @param ConstraintViolationList $violations
     *
     * @return \FOS\RestBundle\View\View
     * @throws ResourceValidationException
     */
    public function createGestionErreur(Livre $livre,ConstraintViolationList $violations)
    {

        if (count($violations)){
            $message = 'The json sent contains invalid data : ';

            foreach ( $violations as $violation){
                $message .= sprintf(
                    "Field %s %s ",
                    $violation->getPropertyPath(),
                    $violation->getMessage()
                );
            }


           throw new ResourceValidationException($message);
        }

        $em = $this->getDoctrine()->getManager();
        $em->persist($livre);
        $em->flush();

        return $this->view(
            $livre,
            Response::HTTP_CREATED,
            [
                'Location' => $this->generateUrl('livre_show',['id' =>$livre->getId()],UrlGeneratorInterface::ABSOLUTE_URL)
            ]
        );

    }


    //Control du parametre envoyé en GET
    /**
     * @Get("/livres", name="livre_list")
     * @QueryParam(
     *     name="order",
     *     requirements="asc|desc",
     *     default="asc",
     *     description="Sort order (asc or desc)"
     * )
     * @View
     */
    public function list(Request $request )
    {
        $order = $request->get('order');
        $livres = $this->getDoctrine()->getRepository(Livre::class)->findAll();
        return $livres;
    }


    //pagination

    /**
     * @Get("/paginatesPagerfanta", name="paginate_pagerfanta_article_list")
     * @QueryParam(
     *     name="keyword",
     *     requirements="[a-zA-Z0-9]",
     *     nullable=true,
     *     description="The keyword to search for."
     * )
     * @QueryParam(
     *     name="order",
     *     requirements="asc|desc",
     *     default="asc",
     *     description="Sort order (asc or desc)"
     * )
     * @QueryParam(
     *     name="limit",
     *     requirements="\d+",
     *     default="15",
     *     description="Max number of movies per page."
     * )
     * @QueryParam(
     *     name="offset",
     *     requirements="\d+",
     *     default="0",
     *     description="The pagination offset"
     * )
     * @View()
     * @param ParamFetcherInterface $paramFetcher
     * @return array
     */
    public function paginatePagerFanta(ParamFetcherInterface $paramFetcher )
    {

        $pager = $this->getDoctrine()->getRepository(Livre::class)->search(
            $paramFetcher->get('keyword'),
            $paramFetcher->get('order'),
            $paramFetcher->get('limit'),
            $paramFetcher->get('offset')
        );

        return $pager->getCurrentPageResults();
    }


    /**
     * @Get("/paginates", name="knppaginate_livre_list")
     * @QueryParam(
     *     name="page",
     *     requirements="\d+",
     *     default="1",
     *     description="The pagination offset"
     * )
     * @View()
     * @param Request $request
     * @param PaginatorInterface $paginator
     * @param ParamFetcherInterface $paramFetcher
     * @return PaginationInterface
     */
    public function paginate(Request $request,PaginatorInterface $paginator,ParamFetcherInterface $paramFetcher )
    {


        $data = $this->getDoctrine()->getRepository(Livre::class)->findAll();


        $articles = $paginator->paginate(
            $data, // Requête contenant les données à paginer (ici nos articles)
            $request->query->getInt('page', 1), // Numéro de la page en cours, passé dans l'URL, 1 si aucune page
            2 // Nombre de résultats par page
        );

        return $articles;
    }


    //Control du parametre envoyé en GET
    /**
     * @Get("/articlesList", name="app_article_list")
     * @RequestParam(
     *     name="search",
     *     requirements="[a-zA-Z0-9]",
     *     default=null,
     *     nullable=true,
     *     description="Search query to look for articles"
     * )
     * @View
     * @param Request $request
     * @param $search
     * @return array
     */
    public function search(Request $request )
    {

        $livres = $this->getDoctrine()->getRepository(Livre::class)->findAll();
        return $livres;

    }



    /**
     * Create a client with a default Authorization header.
     * Cett fonction et celle en dessous sera utilisé dans un autre projet pour acceder au data livre,Article,Author
     *
     * @param string $username
     * @param string $password
     *
     * @return \Symfony\Bundle\FrameworkBundle\Client
     */
    protected function createAuthenticatedClient($username = 'user', $password = 'password')
    {
        $client = static::createAuthenticatedClient();
        $client->request(
            'POST',
            '/api/login_check',
            array(),
            array(),
            array('CONTENT_TYPE' => 'application/json'),
            json_encode(array(
                '_username' => $username,
                '_password' => $password,
            ))
        );

        $data = json_decode($client->getResponse()->getContent(), true);

        $client->setServerParameter('HTTP_Authorization', sprintf('Bearer %s', $data['token']));

        return $client;
    }





    /**
     * test getPagesAction
     */
    public function testGetPages()
    {
        $client = $this->createAuthenticatedClient();
        $client->request('GET', '/api/pages');

    }












}



