<?php



namespace App\Controller;

use App\Entity\Author;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Component\Serializer\Serializer;

class AutherController extends AbstractController
{
    /**
     * @Route("/authers", name="auther_new", methods={"POST"})
     * @param Request $request
     * @return Response
     */
    public function index(Request $request): Response
    {

        $data= $request->getContent();

        $author = $this->get('serializer')->deserialize($data,Author::class,'json');

        $em = $this->getDoctrine()->getManager();
        $em->persist($author);
        $em->flush();

        return new  Response('Author was created',Response::HTTP_CREATED);


    }



    /**
     * @Route("/authers/{id}", name="auther_show", methods={"GET"})
     * @param Request $request
     * @return Response
     */
    public function show(Request $request): Response
    {

        $id= $request->get('id');

        $author= $this->getDoctrine()->getRepository(Author::class)->find($id);

       $data =  $this->get('serializer')->serialize($author ,'json');

        $response = new Response($data);
        $response->headers->set('Content-Type', 'application/json');

        return $response;


    }






}
