<?php

namespace App\Controller;

use App\Entity\Article;
use App\Form\ArticleType;
use App\Repository\ArticleRepository;
use Doctrine\ORM\EntityManager;
use JMS\Serializer\SerializationContext;
use JMS\Serializer\SerializerInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;


class ArticleController extends AbstractController
{

    /**
     * @Route("/articles", name="article_new", methods={"POST"})
     * @param Request $request
     * @param SerializerInterface $serializer
     * @return Response
     */
    public function new(Request $request,SerializerInterface $serializer): Response
    {

            $data =$request->getContent();
           // $article = $this->get("serializer")->deserialize($data, Article::class,'json');

            $article = $serializer->deserialize($data,Article::class,'json');

            $em = $this->getDoctrine()->getManager();
            $em->persist($article);
            $em->flush();


            return new  Response('',Response::HTTP_CREATED);

    }

    /**
     * @Route("/articles/{id}", name="article_show", methods={"GET"})
     * @param Request $request
     * @return Response
     */
    public function show(Request $request,SerializerInterface $serializer){

        $id = $request->get('id');
        $article = $this->getDoctrine()->getRepository(Article::class)->find($id);
       // $data = $this->get("serializer")->serialize($article,'json'); //on passe par le serializer de symfony

        $data= $serializer->serialize($article,'json',SerializationContext::create()->setGroups(array('detail')));

        $response = new Response($data);
        $response->headers->set('Content-Type', 'application/json');

        return $response;

    }


    /**
     * @Route("/articles", name="article", methods={"GET"})
     * @param SerializerInterface $serializer
     * @return Response
     */
    public function list(SerializerInterface $serializer){

        $articles = $this ->getDoctrine()->getRepository(Article::class)->findAll();

        $data = $serializer->serialize($articles,'json',SerializationContext::create()->setGroups(array('list')));

        $response = new Response($data);
        $response->headers->set('Content-Type', 'application/json');

        return $response;

    }






}
