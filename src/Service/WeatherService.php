<?php


namespace App\Service;


use JMS\Serializer\SerializerInterface;
use Psr\Log\LoggerInterface;
use Symfony\Component\DependencyInjection\ParameterBag\ParameterBagInterface;
use Symfony\Component\HttpKernel\Log\Logger;
use Symfony\Contracts\HttpClient\HttpClientInterface;

class WeatherService
{


    /**
     * @var HttpClientInterface
     */
    private $httpClient;

    private $apiKey;

    /**
     * @var SerializerInterface
     */
    private $serialzer;

    public function __construct(HttpClientInterface $httpClient, SerializerInterface $serialzer, ParameterBagInterface $params,LoggerInterface $logger)
    {
        $this->httpClient = $httpClient;
        $this->serialzer = $serialzer;
        $this->apiKey = $params->get('weither_api_key');

        $this->uri = $params->get('API_WEATHER-URI');

        $this->logger= $logger;

    }



    public function getCurrent(){
        $uri =$this->uri."/data/2.5/weather?q=Paris&appid=".$this->apiKey;
        $tab= [];

        try{
            $response = $this->httpClient->request('GET', $uri);
            $data= $this->serialzer->deserialize(

                $response->getContent(),
                "array",
                "json"
            );

            $tab =  [
                'city' =>$data['name'],
                'description' => $data["weather"][0]['main']
            ];

        } catch (\Exception $e){
            $this->logger->error('The weather API returned an error: '.$e->getMessage());
        }

        return $tab;




    }

}