<?php


/**
  @author koby <yahaya.fr>
 */
namespace App\Entity;

use App\Repository\LivreRepository;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;
use Symfony\Component\Validator\Constraints as Assert;
use Hateoas\Configuration\Annotation as Hateoas;



// @Serializer\ExclusionPolicy("ALL")

/**
 * @ORM\Entity(repositoryClass=LivreRepository::class)
 *
 * @Hateoas\Relation(
 *      "self",
 *      href = @Hateoas\Route(
 *          "livre_show",
 *          parameters = { "id" = "expr(object.getId())" },
 *          absolute = true
 *      )
 * )
 *
 * @Hateoas\Relation(
 *      "create",
 *      href = @Hateoas\Route(
 *          "livre_create",
 *          absolute = true
 *      )
 * )
 *
 * @Hateoas\Relation(
 *      "delete",
 *      href = @Hateoas\Route(
 *          "livre_delete",
 *          parameters = { "id" = "expr(object.getId())" },
 *          absolute = true
 *      )
 * )
 *
 * @Hateoas\Relation(
 *      "update",
 *      href = @Hateoas\Route(
 *          "livre_update",
 *          absolute = true
 *      )
 * )
 *
 *  @Hateoas\Relation(
 *      "list",
 *      href = @Hateoas\Route(
 *          "livre_list",
 *          absolute = true
 *      )
 * )
 *
 * @Hateoas\Relation(
 *      "pagination",
 *      href = @Hateoas\Route(
 *          "knppaginate_livre_list",
 *          absolute = true
 *      )
 * )
 *
 *  @Hateoas\Relation(
 *     "author",
 *     embedded = @Hateoas\Embedded("Koby ,yahaya.fr")
 * )
 *
 * @Hateoas\Relation(
 *     "weather",
 *     embedded = @Hateoas\Embedded("expr(service('app.wheather').getCurrent())")
 * )
 *
 *
 *
 * @Hateoas\Relation(
 *     "authenticated_user",
 *     embedded = @Hateoas\Embedded("expr(service('security.token_storage').getToken().getUser())")
 * )
 *
 */
class Livre
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     *
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Assert\NotBlank(groups={"Create"})
     *
     * @Serializer\Since("1.0")
     *
     */
    private $title;

    /**
     * @ORM\Column(type="text")
     * @Assert\NotBlank(groups={"Create"})
     *
     * @Serializer\Since("1.0")
     *
     */
    private $content;

    /**
     * @ORM\Column(type="text", nullable=true)
     * @Assert\NotBlank(groups={"Create"})
     *
     * @Serializer\Since("2.0")
     *
     */
    private $shortDescription;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }

    /**
     * @return mixed
     */
    public function getShortDescription()
    {
        return $this->shortDescription;
    }

    /**
     * @param mixed $shortDescription
     */
    public function setShortDescription($shortDescription): void
    {
        $this->shortDescription = $shortDescription;
    }
}
