<?php

namespace App\Entity;

use App\Repository\ArticleRepository;
use Doctrine\ORM\Mapping as ORM;
use JMS\Serializer\Annotation as Serializer;

// @Serializer\ExclusionPolicy("ALL") l'ensemble des champs de ma ressource ne sont pas visible, se place sur l'entité
// @Serializer\Expose Permet d'exposer un champs (le rendre visible),se place sur l'entité

/**
 * @ORM\Entity(repositoryClass=ArticleRepository::class)
 *
 */
class Article
{
    /**
     * @ORM\Id
     * @ORM\GeneratedValue
     * @ORM\Column(type="integer")
     *@Serializer\Groups({"list"})
     */
    private $id;

    /**
     * @ORM\Column(type="string", length=255)
     * @Serializer\Groups({"list","detail"})
     */
    private $title;

    /**
     * @ORM\Column(type="text")
     * @Serializer\Groups({"list","detail"})
     */
    private $content;

    public function getId(): ?int
    {
        return $this->id;
    }

    public function getTitle(): ?string
    {
        return $this->title;
    }

    public function setTitle(string $title): self
    {
        $this->title = $title;

        return $this;
    }

    public function getContent(): ?string
    {
        return $this->content;
    }

    public function setContent(string $content): self
    {
        $this->content = $content;

        return $this;
    }
}
